import { Component } from '@angular/core';

@Component({
  selector: 'hola-mundo',
  templateUrl: './primer.componente.html',
  styleUrls: ['./primer.componente.css']
})
export class HolaMundo{
  title = "Hola Mundo"
}
