<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class form extends Model{
    public $timestamps = false;
    protected $fillable = ['nombre', 'fono', 'email'];
}
